package com.v3.hq.app;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Logger utility class for writing the data to files
 * 
 * @author vinod
 *
 */
public class LoggerFileWritting {

	private static Log LOGGER = LogFactory.getLog(LoggerFileWritting.class);

	private static final String OS_NAME = "os.name";

	/**
	 * OS name system property.
	 */
	private static final String WINDOWS = "windows";

	/**
	 * path to jboss config folder.
	 */
	private static final String FILE_PREFIX = "file:/";

	/**
	 * file system prefix for unix.
	 */
	private static final String FILE_PREFIX_NO_SLASH = "file:";

	/**
	 * Creates the xml file in jboss.
	 * 
	 * @param fileName
	 *            the file name
	 * @param data
	 *            the data
	 */
	public static void createJsonFile(String fileName, String data, String product) {
		boolean errorFlag = false;
		try {
			String jbossConfigPath = System.getProperty("user.dir");

			LOGGER.info("jbossConfigPath : " + jbossConfigPath);

			// adding date to file name
			fileName = getJsonFileName(fileName, product);
			String filePath = jbossConfigPath;
			String osName = System.getProperty(OS_NAME);

			if (filePath != null) {
				if (osName != null) {
					osName = osName.toLowerCase();
					LOGGER.info("OS Name in lower case: " + osName);
					if (osName.contains(WINDOWS)) {
						if (filePath.contains(FILE_PREFIX)) {
							filePath = filePath.replace(FILE_PREFIX, "");
							filePath = filePath.trim();
						}
					} else {
						if (filePath.contains(FILE_PREFIX_NO_SLASH)) {
							filePath = filePath.replace(FILE_PREFIX_NO_SLASH, "");
							filePath = filePath.trim();
						}
					}
				}
			}

			// Create folder in DEBUG_XML folder . [Example folderDate:
			// 13-June-2012]
			String folderDate = "";

			Date currentDate = new java.util.Date(System.currentTimeMillis());
			// filePath = filePath.replace("conf/", "log/");
			// Create DEBUG_XML folder in jboss-5.1.0.GA\server\default\log\ if
			// doesn't exists.
			File file = new File(filePath + "/" + "DEBUG_XML");
			file.mkdir();

			file = new File(filePath + "/" + "DEBUG_XML/" + product);
			file.mkdir();

			Format formatter = new SimpleDateFormat("dd-MMMM-yyyy"); // [Example
																		// :
																		// 13-June-2012]
			folderDate = formatter.format(currentDate);
			String baseDirectory = filePath + "/" + "DEBUG_XML/" + product + "/" + folderDate;
			new File(baseDirectory).mkdir();

			if (!errorFlag) {
				try {
					filePath = filePath.replaceAll("//", "\\") + "/" + "DEBUG_XML/" + product + "/" + folderDate + "/"
							+ fileName;
					File jbossPath = new File(filePath);

					writeStringToFile(jbossPath, data, "UTF-8");
				} catch (IOException e) {
					LOGGER.error("Error in creating xml file  : " + e.getMessage(), e);
				}
			}
		} catch (Exception ex) {
			errorFlag = true;
			LOGGER.error("Error creating DEBUG_XML folder in : " + ex, ex);
		}
	}

	/**
	 * Creates the xml file in jboss.
	 * 
	 * @param fileName
	 *            the file name
	 * @param data
	 *            the data
	 */
	public static void createJsFile(String fileName, String data, String product) {
		boolean errorFlag = false;
		try {
			String jbossConfigPath = System.getProperty("user.dir");

			LOGGER.info("jbossConfigPath : " + jbossConfigPath);

			// adding date to file name
			fileName = getJsFileName(fileName,product);
			String filePath = jbossConfigPath;
			String osName = System.getProperty(OS_NAME);

			if (filePath != null) {
				if (osName != null) {
					osName = osName.toLowerCase();
					LOGGER.info("OS Name in lower case: " + osName);
					if (osName.contains(WINDOWS)) {
						if (filePath.contains(FILE_PREFIX)) {
							filePath = filePath.replace(FILE_PREFIX, "");
							filePath = filePath.trim();
						}
					} else {
						if (filePath.contains(FILE_PREFIX_NO_SLASH)) {
							filePath = filePath.replace(FILE_PREFIX_NO_SLASH, "");
							filePath = filePath.trim();
						}
					}
				}
			}

			// Create folder in DEBUG_XML folder . [Example folderDate:
			// 13-June-2012]
			String folderDate = "";

			Date currentDate = new java.util.Date(System.currentTimeMillis());
			// filePath = filePath.replace("conf/", "log/");
			// Create DEBUG_XML folder in jboss-5.1.0.GA\server\default\log\ if
			// doesn't exists.
			File file = new File(filePath + "/" + "DEBUG_XML");
			file.mkdir();

			file = new File(filePath + "/" + "DEBUG_XML/" + product);
			file.mkdir();

			Format formatter = new SimpleDateFormat("dd-MMMM-yyyy"); // [Example
																		// :
																		// 13-June-2012]
			folderDate = formatter.format(currentDate);
			String baseDirectory = filePath + "/" + "DEBUG_XML/" + product + "/" + folderDate;
			new File(baseDirectory).mkdir();

			if (!errorFlag) {
				try {
					filePath = filePath.replaceAll("//", "\\") + "/" + "DEBUG_XML/" + product + "/" + folderDate + "/"
							+ fileName;
					File jbossPath = new File(filePath);

					writeStringToFile(jbossPath, data, "UTF-8");
				} catch (IOException e) {
					LOGGER.error("Error in creating xml file  : " + e.getMessage(), e);
				}
			}
		} catch (Exception ex) {
			errorFlag = true;
			LOGGER.error("Error creating DEBUG_XML folder in : " + ex, ex);
		}
	}

	public static void writeStringToFile(File file, String data, String encoding) throws IOException {
		writeStringToFile(file, data, encoding, false);
	}

	/**
	 * Writes a String to a file creating the file if it does not exist.
	 *
	 * @param file
	 *            the file to write
	 * @param data
	 *            the content to write to the file
	 * @param encoding
	 *            the encoding to use, <code>null</code> means platform default
	 * @param append
	 *            if <code>true</code>, then the String will be added to the end of
	 *            the file rather than overwriting
	 * @throws IOException
	 *             in case of an I/O error
	 * @throws java.io.UnsupportedEncodingException
	 *             if the encoding is not supported by the VM
	 * @since 2.1
	 */
	public static void writeStringToFile(File file, String data, String encoding, boolean append) throws IOException {
		OutputStream out = null;
		try {
			out = openOutputStream(file, append);
			write(data, out, encoding);
			out.close(); // don't swallow close Exception if copy completes normally
		} finally {
			closeQuietly(out);
		}
	}
	
	/**
     * Unconditionally close a <code>Closeable</code>.
     * <p>
     * Equivalent to {@link Closeable#close()}, except any exceptions will be ignored.
     * This is typically used in finally blocks.
     * <p>
     * Example code:
     * <pre>
     *   Closeable closeable = null;
     *   try {
     *       closeable = new FileReader("foo.txt");
     *       // process closeable
     *       closeable.close();
     *   } catch (Exception e) {
     *       // error handling
     *   } finally {
     *       IOUtils.closeQuietly(closeable);
     *   }
     * </pre>
     *
     * @param closeable the object to close, may be null or already closed
     * @since 2.0
     */
	public static void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ioe) {
			// ignore
		}
	}
	
	 /**
     * Writes chars from a <code>String</code> to bytes on an
     * <code>OutputStream</code> using the specified character encoding.
     * <p>
     * Character encoding names can be found at
     * <a href="http://www.iana.org/assignments/character-sets">IANA</a>.
     * <p>
     * This method uses {@link String#getBytes(String)}.
     * 
     * @param data  the <code>String</code> to write, null ignored
     * @param output  the <code>OutputStream</code> to write to
     * @param encoding  the encoding to use, null means platform default
     * @throws NullPointerException if output is null
     * @throws IOException if an I/O error occurs
     * @since 1.1
     */
	public static void write(String data, OutputStream output, String encoding) throws IOException {
		if (data != null) {
			if (encoding == null) {
				write(data, output);
			} else {
				output.write(data.getBytes(encoding));
			}
		}
	}
	
	 /**
     * Writes chars from a <code>String</code> to bytes on an
     * <code>OutputStream</code> using the default character encoding of the
     * platform.
     * <p>
     * This method uses {@link String#getBytes()}.
     * 
     * @param data  the <code>String</code> to write, null ignored
     * @param output  the <code>OutputStream</code> to write to
     * @throws NullPointerException if output is null
     * @throws IOException if an I/O error occurs
     * @since 1.1
     */
	public static void write(String data, OutputStream output) throws IOException {
		if (data != null) {
			output.write(data.getBytes());
		}
	}
	
	/**
     * Opens a {@link FileOutputStream} for the specified file, checking and
     * creating the parent directory if it does not exist.
     * <p>
     * At the end of the method either the stream will be successfully opened,
     * or an exception will have been thrown.
     * <p>
     * The parent directory will be created if it does not exist.
     * The file will be created if it does not exist.
     * An exception is thrown if the file object exists but is a directory.
     * An exception is thrown if the file exists but cannot be written to.
     * An exception is thrown if the parent directory cannot be created.
     * 
     * @param file  the file to open for output, must not be <code>null</code>
     * @param append if <code>true</code>, then bytes will be added to the
     * end of the file rather than overwriting
     * @return a new {@link FileOutputStream} for the specified file
     * @throws IOException if the file object is a directory
     * @throws IOException if the file cannot be written to
     * @throws IOException if a parent directory needs creating but that fails
     * @since 2.1
     */
	public static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
		if (file.exists()) {
			if (file.isDirectory()) {
				throw new IOException("File '" + file + "' exists but is a directory");
			}
			if (file.canWrite() == false) {
				throw new IOException("File '" + file + "' cannot be written to");
			}
		} else {
			File parent = file.getParentFile();
			if (parent != null) {
				if (!parent.mkdirs() && !parent.isDirectory()) {
					throw new IOException("Directory '" + parent + "' could not be created");
				}
			}
		}
		return new FileOutputStream(file, append);
	}

	/**
	 * Gets the date file name.
	 * 
	 * @param fileName
	 *            the file name
	 * @return the date file name
	 */
	public static String getJsonFileName(String fileName, String product) {
		if(null != product && !product.equals("CT_ROE")) {
			Date currentDate = new java.util.Date(System.currentTimeMillis());
			Format formatter = new SimpleDateFormat("dd-MMMM-yyyy-hh-mm-ss");
			String date = formatter.format(currentDate);
			fileName = fileName + "@" + date + " " + currentDate.getTime() + ".json";
		}else {
			fileName = fileName + ".json";
		}
		return fileName;
	}

	/**
	 * Gets the date file name.
	 * 
	 * @param fileName
	 *            the file name
	 * @return the date file name
	 */
	public static String getJsFileName(String fileName, String product) {
		if(null != product && !product.equals("CT_ROE")) {
			Date currentDate = new java.util.Date(System.currentTimeMillis());
			Format formatter = new SimpleDateFormat("dd-MMMM-yyyy-hh-mm-ss");
			String date = formatter.format(currentDate);
			fileName = fileName + "@" + date + " " + currentDate.getTime() + ".js";
		}else {
			fileName = fileName + ".js";
		}
		return fileName;
	}

}
