package com.v3.hq.dao;

import java.util.List;

import com.v3.hq.company.pojo.AddEmploye;
import com.v3.hq.company.pojo.CompanyDomain;


public interface HqPeopleDao {

	void saveEmp(AddEmploye addEmploye);

	List<Object[]> loadAllCompanies();

	void saveCompany(CompanyDomain addEmploye);

}
