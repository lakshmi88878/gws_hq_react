package com.v3.hq.dao;

import java.text.SimpleDateFormat;
import java.util.Date;



import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.v3.hq.company.pojo.AddEmploye;
import com.v3.hq.company.pojo.CompanyDomain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
@Repository
public class HqPeopleDaoImpl extends GenericHibernateDAOImpl implements HqPeopleDao{
	private static Log LOGGER = LogFactory.getLog(HqPeopleDaoImpl.class);
	
	@Override
	public void saveEmp(AddEmploye addEmploye){
		Date nowDate=new Date();
		String seqNumber=null;
		List<Object[]> results = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String currentDate = formatter.format(addEmploye.getStartDate());
		StringBuilder queryBuilder1 = new StringBuilder();
		queryBuilder1.append("select TEST_EMP_REACT_SEQ.nextval FROM dual");
		results = executeSQL(queryBuilder1.toString());
		if(!CollectionUtils.isEmpty(results)){
			Object[] data = new Object[1];
			data[0] = results.get(0);
			if (data[0] != null) {
				seqNumber = data[0].toString();
				LOGGER.debug("executeSQL start :  seqence : "+  seqNumber);
			}
		}
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("INSERT INTO EMP_TEST_REACT (ID,FNAME,LNAME,EMAIL,CONTACT_NUM,NATIONALITY,GENDER,TITLE,DOB,COMMENTS) VALUES(");
		sqlBuilder.append(Long.valueOf(seqNumber)+",");
		sqlBuilder.append("'"+addEmploye.getEmpFirstName()+"',");
		sqlBuilder.append("'"+addEmploye.getEmpLastName()+"',");
		sqlBuilder.append("'"+addEmploye.getEmpEmailId()+"', ");
		sqlBuilder.append("'"+addEmploye.getContactNum()+"', ");
		sqlBuilder.append("'"+addEmploye.getNationality()+"', ");
		sqlBuilder.append("'"+addEmploye.getGender()+"', ");
		sqlBuilder.append("'"+addEmploye.getTitle()+"', ");
		sqlBuilder.append(" TO_DATE('"+currentDate+"','DD-MON-YYYY HH24:MI:SS') , ");
		sqlBuilder.append("'"+addEmploye.getComment()+"' ");
		sqlBuilder.append(") ");
		
		LOGGER.debug("saveEmp SQL:  " + sqlBuilder.toString());
		
		Integer status=saveOrUpdateQuery(sqlBuilder.toString());
		LOGGER.debug("saveEmp :  " + status);
	}
	
	@Override
	public void saveCompany(CompanyDomain addEmploye){
		String seqNumber=null;
		List<Object[]> results = null;
		StringBuilder queryBuilder1 = new StringBuilder();
		queryBuilder1.append("select TEST_EMP_REACT_SEQ.nextval FROM dual");
		results = executeSQL(queryBuilder1.toString());
		if(!CollectionUtils.isEmpty(results)){
			Object[] data = new Object[1];
			data[0] = results.get(0);
			if (data[0] != null) {
				seqNumber = data[0].toString();
				LOGGER.debug("executeSQL start :  seqence : "+  seqNumber);
			}
		}
		String domainName="Ebrrez";
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("INSERT INTO REACT_COMP_DOMAIN (ID,COMPANY,DOMAIN) VALUES(");
		sqlBuilder.append(Long.valueOf(seqNumber)+",");
		sqlBuilder.append("'"+domainName+"',");
		sqlBuilder.append("'"+addEmploye.getDomain()+"' ");
		sqlBuilder.append(") ");
		LOGGER.debug("saveCompany SQL:  " + sqlBuilder.toString());
		Integer status=saveOrUpdateQuery(sqlBuilder.toString());
		LOGGER.debug("saveCompany :  " + status);
	}
	
	
	@Override
	 public  List<Object[]> loadAllCompanies(){
	    	LOGGER.debug(" ReactPeopleDaoImpl loadAllCompanies startd ");
	    	List<Object[]> results = null;
	    	Object[] obj=null;
	    		try {
	    		StringBuilder queryBuilder = new StringBuilder();
	    		queryBuilder.append("SELECT ID,COMPANY,DOMAIN FROM REACT_COMP_DOMAIN");
	    		results = executeSQL(queryBuilder.toString());
	    	} catch (Exception re) {
	    		LOGGER.error("DataAccessException occured in ReactPeopleDaoImpl  loadAllCompanies :"+ re, re);
	    	}
	    	LOGGER.debug(" ReactPeopleDaoImpl loadAllCompanies ended ");
	    	return results;
	    }
}
