package com.v3.hq.dao;

import java.util.List;

import org.hibernate.Session;

/**
 * @author Nagaram vinod Reddy
 * @param <T>
 *
 */
public interface GenericHibernateDAO<T> {

	/**
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	List<Object[]> executeSQL(String sql) throws Exception;

	/**
	 * @param queryString
	 * @return
	 */
	Integer saveOrUpdateQuery(String queryString);

	/**
	 * @return
	 */
	Session getSession();

	/**
	 * @param entity
	 */
	void save(T entity);

	/**
	 * @param entity
	 */
	void update(T entity);

}
