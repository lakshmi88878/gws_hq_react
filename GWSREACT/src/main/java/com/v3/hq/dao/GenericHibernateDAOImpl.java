package com.v3.hq.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

/**
 * This is Generic Hibernate implementation 
 * 
 * @author Nagaram vinod Reddy
 *
 */
@Repository
public abstract class GenericHibernateDAOImpl<T> implements GenericHibernateDAO<T>{
	
	

	private static Log LOGGER = LogFactory.getLog(GenericHibernateDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	/**
	 * @return
	 */
	public Session getSession() {
		Session session = null;
		
		session = this.sessionFactory.getCurrentSession(); 
		
		return session;
	}
	
	
	public void save(T entity) {
		try {
			getSession().save(entity);
		} catch (DataAccessException da) {
			LOGGER.error(
					"GenericHibernateDAOImpl save DataAccessException occured :" + da,da);
			throw da;
		}

	}
	
	
	public void update(T entity) {
		try {
			getSession().update(entity);
		} catch (DataAccessException da) {
			LOGGER.error(
					"GenericHibernateDAOImpl save DataAccessException occured :" + da,da);
			throw da;
		}

	}
	
	/**
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	
	public List<Object[]> executeSQL(String sql) {
		LOGGER.debug("executeSQL start");
		Session session = getSession(); 
		List<Object[]> results = null;	
		
		Query query = session.createNativeQuery(sql);
		results = query.list();
		
		LOGGER.debug("executeSQL end");
		return results;
	}
	
	/**
	 * @param queryString
	 * @return
	 */
	public Integer saveOrUpdateQuery(String queryString) {
		LOGGER.debug("saveOrUpdateQuery method start: "+queryString);
		Integer results = null;
		Session session = getSession();    
		
		Query query = session.createNativeQuery(queryString);
		results= query.executeUpdate();
		
		LOGGER.debug("saveOrUpdateQuery method end");
		return results;
	}
	
	
	

}
