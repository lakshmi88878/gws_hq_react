package com.v3.hq.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.v3.hq.company.pojo.SignInRespHeader;
import com.v3.hq.company.pojo.UserSignInReq;
import com.v3.hq.company.pojo.UserSignInResp;
import com.v3.hq.service.HqUserSignServiceImpl;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HqUserSignInController {
	
	private static Log LOGGER = LogFactory.getLog(HqUserSignInController.class);
	
	@Autowired
	private HqUserSignServiceImpl hqUserSignServiceImpl;
	
	@RequestMapping(value = "/checkUsrLogin", method = RequestMethod.POST, consumes = "application/json")
	public SignInRespHeader checkUserLogin(@RequestBody String request,
			UriComponentsBuilder ucBuilder) {
		LOGGER.debug("checkUserAuthentication start");
		String status="";
		ObjectMapper mapper=new ObjectMapper();
		HttpResponse signInResp=null;
		SignInRespHeader inRespHeader=new SignInRespHeader();		
		try {
			if (request != null) {
				UserSignInReq signInReq=mapper.readValue(request, UserSignInReq.class);
				signInReq.setRedirect("true");
				signInReq.setService("/hq/company");
				signInReq.setSource("ui");
				signInReq.setCaller("hq");
				signInResp=hqUserSignServiceImpl.checkUsrAuthentication(signInReq);
			   }
			   if(signInResp!=null){
				   String res = EntityUtils.toString(signInResp.getEntity());
				   UserSignInResp ctRetriveResp = mapper.readValue(res, UserSignInResp.class);
				   
				   if(!StringUtils.isEmpty(ctRetriveResp.getRedirect())){
				   status="Success";
				   inRespHeader.setRedirect(ctRetriveResp.getRedirect());
				   }else{
					   if(ctRetriveResp.getFault()!=null && !StringUtils.isEmpty(ctRetriveResp.getFault().getFault_code())){
						   String resCode=ctRetriveResp.getFault().getFault_code();
						   if(resCode.equalsIgnoreCase("2")){
							   status="Incorrect Password";
						   }else if(resCode.equalsIgnoreCase("21")){
							   status="OTP Expected";
						   }else if(resCode.equalsIgnoreCase("20")){
							   status="OTP verification failed";
						   }else if(resCode.equalsIgnoreCase("1")){
							   status="User name not matched";
						   }
					   }
				   }
				   inRespHeader.setStatus(status);
				   if(status.equalsIgnoreCase("Success")){
				   Header[] headers = signInResp.getAllHeaders();
				   for (Header header : headers) {
					   	if(header.getValue().contains("ct-auth=")){
							inRespHeader.setCtAuthValue(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("usermisc=")){
							inRespHeader.setUsermiscValue(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("userid=")){
							inRespHeader.setUseridValue(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("ct-auth-preferences=")){
							inRespHeader.setPreferencesValue(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("currency-pref=")){
							inRespHeader.setCurrencyprefValue(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("_session_id=")){
							inRespHeader.setSessionId(splitHeaderValue(header.getValue()));
						}else if(header.getValue().contains("Apache=")){
							inRespHeader.setApacheValue(splitHeaderValue(header.getValue()));
						}
					}
				   }
			   }else{
				   status="500 - Internal Server Error";
			   }
		} catch (Exception e) {
			LOGGER.error(
					"Exception in checkUserAuthentication due to : " + e,
					e);
		}
		
		LOGGER.debug("checkUserAuthentication end");
		return inRespHeader;
	}
	
	/**
	 * @param value
	 * @return
	 */
	public String splitHeaderValue(String value){
		String headeValue="";
		try{
			Object[] obj=value.split("=");
			String auth=obj[1].toString();
			if(auth!=null){
				Object[] objValue=auth.split(";");
				if(objValue !=null && objValue[0]!=null){
				headeValue=objValue[0].toString();
				}
				LOGGER.debug("User Authentication Headers : VALUE :"+headeValue);
			}
		}catch (Exception e) {
			LOGGER.error(
					"Exception in splitHeader due to : " + e,
					e);
		}
		return headeValue;
		
		
		
	}
	

}
