/**
 * 
 */
package com.v3.hq.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.v3.hq.company.pojo.Company;
import com.v3.hq.company.pojo.CompanyConfig;
import com.v3.hq.company.pojo.CompanyDetails;
import com.v3.hq.company.pojo.CompanyProfileResponse;
import com.v3.hq.company.pojo.GenericResponse;
import com.v3.hq.company.pojo.GstDelete;
import com.v3.hq.company.pojo.GstDetailsUpdate;
import com.v3.hq.company.pojo.GstResponse;
import com.v3.hq.company.pojo.GstSearch;
import com.v3.hq.service.HqCompanyService;

/**
 * @author Rakesh Korwar
 *
 */

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HqCompanyController {
	
private static Log LOGGER = LogFactory.getLog(HqPeopleController.class);
	
	@Autowired
	private HqCompanyService hqCompanyService;
	
	@RequestMapping(value = "/getCmpConfAndGstDtls", method = RequestMethod.POST, consumes = "application/json")
	public CompanyProfileResponse getCompanyConfigAndGstDetailsByCompanyId(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("getCompanyConfigAndGstDetailsByCompanyId START");
		
		CompanyProfileResponse resp = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				GstSearch gst = mapper.readValue(request, GstSearch.class);
				resp = hqCompanyService.getCmpPrfByCmpId(gst.getCompanyId());
				LOGGER.debug("Response : " + resp);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in getCompanyConfigAndGstDetailsByCompanyId due to : " + e, e);
		}
		
		LOGGER.debug("getCompanyConfigAndGstDetailsByCompanyId END");
		return resp;
	}
	
	@RequestMapping(value = "/getCmpConfByDomain", method = RequestMethod.POST, consumes = "application/json")
	public CompanyProfileResponse getCompanyConfigDetailsByDomain(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("getCompanyConfigDetailsByDomain START");
		
		CompanyProfileResponse resp = new CompanyProfileResponse();
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				GstSearch cpd = mapper.readValue(request, GstSearch.class);
				resp = hqCompanyService.getCmpPrfByDomain(cpd.getDomainName());
				LOGGER.debug("Response : " + resp);
			}
		} catch (Exception e) {
			resp.setStatus(400);
			LOGGER.error("Exception in getCompanyConfigDetailsByDomain due to : " + e, e);
		}
		
		LOGGER.debug("getCompanyConfigDetailsByDomain END");
		return resp;
	}
	
	@RequestMapping(value = "/updateCompConf", method = RequestMethod.POST, consumes = "application/json")
	public CompanyProfileResponse updateCompanyConfig(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("updateCompanyConfig START");
		
		CompanyProfileResponse resp = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				CompanyDetails cd = mapper.readValue(request, CompanyDetails.class);
				Company cmpCnf = cd.getCompany();
				
				resp = hqCompanyService.updateCmpContInfo(cmpCnf, cd.getCompanyId());
				
				cmpCnf.setContactData(null);
				if(cmpCnf != null && !CollectionUtils.isEmpty(cmpCnf.getCompanyConfigs())){
					List<CompanyConfig> listData = cmpCnf.getCompanyConfigs().stream().filter(item -> item.getConfigName() != null && item.getConfigName() !="" &&
							item.getConfigValue() != null && item.getConfigValue() !="").collect(Collectors.toList());
					cmpCnf.setCompanyConfigs(listData);
				}
				resp = hqCompanyService.addEditCompConf(cd, cd.getCompanyId());
				LOGGER.debug("Response : " + resp);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in updateCompanyConfig due to : " + e, e);
		}
		
		LOGGER.debug("updateCompanyConfig END");
		return resp;
	}
	
	@RequestMapping(value = "/getCompanyGstDtls", method = RequestMethod.POST, consumes = "application/json")
	public GstResponse getCompanyGstDetails(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("getCompanyGstDetails START");
		
		GstResponse gst = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				GstSearch cpd = mapper.readValue(request, GstSearch.class);
				gst = hqCompanyService.companyGstDetails(cpd.getDomainName(), cpd.getCompanyId());
				LOGGER.debug("Response : " + gst);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in getCompanyGstDetails due to : " + e, e);
		}
		
		LOGGER.debug("getCompanyGstDetails END");
		return gst;
	}
	
	@RequestMapping(value = "/updateCmpGstDetails", method = RequestMethod.POST, consumes = "application/json")
	public GenericResponse updateCmpGstDetails(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("updateCmpGstDetails START");
		
		GenericResponse resp = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				GstDetailsUpdate gstUpd = mapper.readValue(request, GstDetailsUpdate.class);
				resp = hqCompanyService.updateCompanyGst(gstUpd);
				LOGGER.debug("Response : " + resp);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in updateCmpGstDetails due to : " + e, e);
		}
		
		LOGGER.debug("updateCmpGstDetails END");
		return resp;
	}
	
	@RequestMapping(value = "/cmpGstDelete", method = RequestMethod.POST, consumes = "application/json")
	public GenericResponse deleteCmpGst(@RequestBody String request, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("deleteCmpGst START");
		
		GenericResponse resp = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (request != null) {
				GstDelete gstDel = mapper.readValue(request, GstDelete.class);
				resp = hqCompanyService.gstDelete(gstDel);
				LOGGER.debug("Response : " + resp);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in deleteCmpGst due to : " + e, e);
		}
		
		LOGGER.debug("deleteCmpGst END");
		return resp;
	}

}
