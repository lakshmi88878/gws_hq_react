package com.v3.hq.controllers;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.v3.hq.company.pojo.AddEmploye;
import com.v3.hq.company.pojo.CompanyDomain;
import com.v3.hq.service.HqPeopleServiceImpl;
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HqPeopleController{
	
	private static Log LOGGER = LogFactory.getLog(HqPeopleController.class);
	
	@Autowired
	private HqPeopleServiceImpl mySpringBootServiceImpl;
	
	
	@RequestMapping(value = "/addEmploye", method = RequestMethod.POST, consumes = "application/json")
	public void processMySpringBootApp(@RequestBody String request,
			UriComponentsBuilder ucBuilder) {
		LOGGER.debug("processMySpringBootApp start");
		ObjectMapper mapper=new ObjectMapper();
		try {
			if (request != null) {
				AddEmploye emp=mapper.readValue(request, AddEmploye.class);
				mySpringBootServiceImpl.saveEmploye(emp);
				LOGGER.debug("processMySpringBootApp saved..........");
			   }
		} catch (Exception e) {
			LOGGER.error(
					"Exception in processMySpringBootApp due to : " + e,
					e);
		}
		LOGGER.debug("processMySpringBootApp end");
	}
	
	@RequestMapping(value = "/addCompany", method = RequestMethod.POST, consumes = "application/json")
	public void saveCompany(@RequestBody String request,UriComponentsBuilder ucBuilder) {
		LOGGER.debug("saveCompany start");
		ObjectMapper mapper=new ObjectMapper();
		try {
			if (request != null) {
				CompanyDomain emp=mapper.readValue(request, CompanyDomain.class);
				mySpringBootServiceImpl.saveCompany(emp);
				LOGGER.debug("saveCompany saved..........");
			   }
		} catch (Exception e) {
			LOGGER.error(
					"Exception in saveCompany due to : " + e,
					e);
		}
		LOGGER.debug("saveCompany end");
	}
	
	
	@RequestMapping(value = "/loadCompanyData", method = RequestMethod.GET)
	public List<CompanyDomain> loadAllCompanys() {
		LOGGER.debug("processMySpringBootApp start");
		List<CompanyDomain> compList=new ArrayList<CompanyDomain>();
		try {
			compList=mySpringBootServiceImpl.loadCompanies();
			LOGGER.debug("processMySpringBootApp saved..........");
		} catch (Exception e) {
			LOGGER.error(
					"Exception in processMySpringBootApp due to : " + e,
					e);
		}
		LOGGER.debug("processMySpringBootApp end");
		return compList;
	}
	
	
}
