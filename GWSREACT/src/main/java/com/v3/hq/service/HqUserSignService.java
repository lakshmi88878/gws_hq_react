package com.v3.hq.service;

import org.apache.http.HttpResponse;

import com.v3.hq.company.pojo.UserSignInReq;
import com.v3.hq.company.pojo.UserSignInResp;

public interface HqUserSignService {

	/**
	 * @param signInReq
	 * @return
	 */
	HttpResponse checkUsrAuthentication(UserSignInReq signInReq);

}
