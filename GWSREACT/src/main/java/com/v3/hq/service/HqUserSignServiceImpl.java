package com.v3.hq.service;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.v3.hq.company.pojo.UserSignInReq;
import com.v3.hq.company.pojo.UserSignInResp;

@Service
@Transactional
public class HqUserSignServiceImpl implements HqUserSignService,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Log LOGGER = LogFactory.getLog(HqUserSignServiceImpl.class);
	
	@Autowired
	private Environment env;
	
	
	/**
	 * @param signInReq
	 * @return
	 */
	
	public HttpResponse checkUsrAuthentication(UserSignInReq signInReq) {
		HttpResponse responseBody=null;
		try {
			LOGGER.info("HqUserSignServiceImpl usrAuthenticationResp start");
			ObjectMapper mapper = new ObjectMapper();
			String request = mapper.writeValueAsString(signInReq);
			LOGGER.info("Usr Authentication REQ "+"-> "+ request);
			HttpClient httpClient =  HttpClientBuilder.create().build();
			int timeout = 3000;
			StringEntity requestEntity = new StringEntity(request, StandardCharsets.UTF_8);
			requestEntity.setContentType("application/json");
			String searchURL = env.getProperty("hq.company.signIn.req.url");
			HttpPost postMethod = new HttpPost(searchURL);
			RequestConfig requestConfig = RequestConfig.custom()
					  .setSocketTimeout(timeout * 1000)
					  .setConnectTimeout(timeout * 1000)
					  .setConnectionRequestTimeout(timeout * 1000)
					  .build();
			postMethod.setConfig(requestConfig);
			postMethod.setEntity(requestEntity);
			postMethod.addHeader("Accept", "text/json");
			postMethod.addHeader("X-Forwarded-Proto", "https");
		    responseBody = httpClient.execute(postMethod);
			LOGGER.info("HqUserSignServiceImpl usrAuthenticationResp start");
		} catch (Exception e) {
			LOGGER.error("Exception in HqUserSignServiceImpl usrAuthenticationResp  due to : " + e, e);
		}
		return responseBody;

	}

}
