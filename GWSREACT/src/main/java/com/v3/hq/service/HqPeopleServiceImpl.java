package com.v3.hq.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.v3.hq.company.pojo.AddEmploye;
import com.v3.hq.company.pojo.CompanyDomain;
import com.v3.hq.dao.HqPeopleDao;

@Service
@Transactional
public class HqPeopleServiceImpl implements HqPeopleService, Serializable{
	
	private static Log LOGGER = LogFactory.getLog(HqPeopleServiceImpl.class);
	
	@Autowired
	private HqPeopleDao mySpringBootDao;
	
	
	/**
	 * @param addEmploye
	 */
	public void saveEmploye(AddEmploye addEmploye){
		LOGGER.debug("FlightCTShedularServiceImpl saveEmploye start");
		mySpringBootDao.saveEmp(addEmploye);
		LOGGER.debug("FlightCTShedularServiceImpl saveEmploye end");
	}
	

	/**
	 * @param addEmploye
	 */
	public void saveCompany(CompanyDomain addEmploye){
		LOGGER.debug("FlightCTShedularServiceImpl saveCompany start");
		mySpringBootDao.saveCompany(addEmploye);
		LOGGER.debug("FlightCTShedularServiceImpl saveCompany end");
	}
	
	public List<CompanyDomain> loadCompanies(){
		List<CompanyDomain> companyList=new ArrayList<CompanyDomain>();
		try{
		LOGGER.debug("FlightCTShedularServiceImpl saveEmploye start");
		List<Object[]> compList=mySpringBootDao.loadAllCompanies();
		if(!CollectionUtils.isEmpty(compList)){
			for(Object[] com:compList){
				CompanyDomain companyDomain=new CompanyDomain();
				BigDecimal id=(BigDecimal) com[0];
				companyDomain.setCompanyId(id.longValue());
				companyDomain.setCompany(com[1].toString());
				companyDomain.setDomain(com[2].toString());
				companyList.add(companyDomain);
			}
		}
		LOGGER.debug("FlightCTShedularServiceImpl saveEmploye end");
		
		}catch (Exception re) {
    		LOGGER.error("DataAccessException occured in FlightCTBookingDetailsDaoImpl  getFltNotConfiremdData :"+ re, re);
    	}
		return companyList;
	}

	
}
