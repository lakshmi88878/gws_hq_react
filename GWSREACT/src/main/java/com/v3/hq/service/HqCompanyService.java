package com.v3.hq.service;

import com.v3.hq.company.pojo.Company;
import com.v3.hq.company.pojo.CompanyDetails;
import com.v3.hq.company.pojo.CompanyProfileResponse;
import com.v3.hq.company.pojo.ContactData;
import com.v3.hq.company.pojo.GenericResponse;
import com.v3.hq.company.pojo.GstDelete;
import com.v3.hq.company.pojo.GstDetailsUpdate;
import com.v3.hq.company.pojo.GstResponse;

/**
 * @author Rakesh Korwar
 *
 */

public interface HqCompanyService {
	
	CompanyProfileResponse addEditCompConf(CompanyDetails cmp, String companyId);

	CompanyProfileResponse getCmpPrfByCmpId(Integer cdu);

	CompanyProfileResponse getCmpPrfByDomain(String domainName);

	CompanyProfileResponse updateCmpContInfo(Company cmpCnf, String companyId);

	GenericResponse gstDelete(GstDelete gstDel);

	GenericResponse updateCompanyGst(GstDetailsUpdate gstUpd);

	GstResponse companyGstDetails(String domainName, Integer companyId);

}
