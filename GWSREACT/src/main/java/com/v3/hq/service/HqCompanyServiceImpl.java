package com.v3.hq.service;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.v3.hq.company.pojo.Company;
import com.v3.hq.company.pojo.CompanyConfigUpdate;
import com.v3.hq.company.pojo.CompanyDetails;
import com.v3.hq.company.pojo.CompanyProfileResponse;
import com.v3.hq.company.pojo.ContactData;
import com.v3.hq.company.pojo.GenericResponse;
import com.v3.hq.company.pojo.GstDelete;
import com.v3.hq.company.pojo.GstDetails;
import com.v3.hq.company.pojo.GstDetailsUpdate;
import com.v3.hq.company.pojo.GstResponse;
import com.v3.hq.company.pojo.GstSearch;
import com.v3.hq.controllers.HqPeopleController;

/**
 * @author Rakesh Korwar
 *
 */

@Service
@Transactional
public class HqCompanyServiceImpl implements HqCompanyService {
	
private static Log LOGGER = LogFactory.getLog(HqPeopleController.class);

@Autowired
private Environment env;

	@Override
	public CompanyProfileResponse addEditCompConf(CompanyDetails cmp, String companyId) {
		LOGGER.debug("addEditCompConf START");
		
		CompanyProfileResponse resp = new CompanyProfileResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			
			CompanyDetails newObj = new CompanyDetails();
			newObj.setCompany(cmp.getCompany());
			String jsonRq = mapper.writeValueAsString(newObj);
			
			HttpPost httpPost = new HttpPost(env.getProperty("company.conf.add.edit.api") + companyId);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpPost.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			StringEntity requestEntity = new StringEntity(jsonRq, ContentType.create("application/json", "UTF-8"));
			httpPost.setHeader(null); //env.getProperty("ct.currency.scheduler.auth.key"), env.getProperty("ct.currency.scheduler.auth.value")
			       
			httpPost.setEntity(requestEntity);
			httpPost.setHeader("Accept", "text/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpPost, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl addEditCompConf Response : " + responseBody);
			
			resp = mapper.readValue(responseBody, CompanyProfileResponse.class);
			resp.setMsg("SUCCESS");
			resp.setStatus(200);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in addEditCompConf due to : " + e, e);
			resp.setMsg("Company Config update failed");
			resp.setStatus(404);
		}
		
		LOGGER.debug("addEditCompConf END");
		return resp;
	}

	@Override
	public CompanyProfileResponse getCmpPrfByCmpId(Integer cmpId) {
		LOGGER.debug("getCmpPrfByCmpId START");
		
		CompanyProfileResponse resp = new CompanyProfileResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			
			HttpGet httpget = new HttpGet(env.getProperty("company.conf.search.companyId") + cmpId);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpget.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			httpget.setHeader("Content-type", "application/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpget, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl addEditCompConf Response : " + responseBody);
			
			resp = mapper.readValue(responseBody, CompanyProfileResponse.class);
			resp.setMsg("SUCCESS");
			resp.setStatus(200);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in getCmpPrfByCmpId due to : " + e, e);
			resp.setMsg("Searched Company ID does not exist");
			resp.setStatus(404);
		}
		
		LOGGER.debug("getCmpPrfByCmpId END");
		return resp;
	}

	@Override
	public CompanyProfileResponse getCmpPrfByDomain(String domainName) {
		LOGGER.debug("getCmpPrfByDomain START");
		
		CompanyProfileResponse resp = new CompanyProfileResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			
			HttpGet httpGet = new HttpGet(env.getProperty("company.conf.search.domain") + domainName + env.getProperty("company.conf.search.domain.caller"));
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpGet.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			httpGet.setHeader("Accept", "text/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpGet, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl getCmpPrfByDomain Response : " + responseBody);
			
			resp = mapper.readValue(responseBody, CompanyProfileResponse.class);
			resp.setMsg("SUCCESS");
			resp.setStatus(200);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in getCmpPrfByDomain due to : " + e, e);
			resp.setMsg("Searched Domain does not exist");
			resp.setStatus(404);
		}
		LOGGER.debug("getCmpPrfByDomain END");
		return resp;
	}

	@Override
	public CompanyProfileResponse updateCmpContInfo(Company cntData, String companyId) {
		LOGGER.debug("updateCmpContInfo START");
		
		CompanyProfileResponse resp = new CompanyProfileResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			
			Company newObj = new Company();
			newObj.setContactData(cntData.getContactData());
			String jsonRq = mapper.writeValueAsString(newObj);
			
			HttpPost httpPost = new HttpPost(env.getProperty("update.company.contact.dtls1") + companyId + env.getProperty("update.company.contact.dtls2"));
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpPost.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			StringEntity requestEntity = new StringEntity(jsonRq, ContentType.create("application/json", "UTF-8"));
			httpPost.setHeader(null); //env.getProperty("ct.currency.scheduler.auth.key"), env.getProperty("ct.currency.scheduler.auth.value")
			       
			httpPost.setEntity(requestEntity);
			httpPost.setHeader("Accept", "text/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpPost, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl updateCmpContInfo Response : " + responseBody);
			
			resp = mapper.readValue(responseBody, CompanyProfileResponse.class);
			resp.setMsg("SUCCESS");
			resp.setStatus(200);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in updateCmpContInfo due to : " + e, e);
			resp.setMsg("Company Contact update failed");
			resp.setStatus(404);
		}
		
		LOGGER.debug("addEditCompConf END");
		return resp;
	}

	@Override
	public GstResponse companyGstDetails(String domainName, Integer companyId) {
		LOGGER.debug("companyGstDetails START");
		
		GstResponse resp = new GstResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			
			HttpGet httpGet = null;
			if(null != domainName) {
				httpGet = new HttpGet(env.getProperty("company.gst.search.domain") + domainName);	
			}else if(null != companyId) {
				httpGet = new HttpGet(env.getProperty("company.gst.search.companyId") + companyId);
			}
			
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpGet.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			httpGet.setHeader("Content-type", "application/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpGet, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl companyGstDetails Response : " + responseBody);
			
			GstResponse gstResp = mapper.readValue(responseBody, GstResponse.class);
			resp.setGstDetails(gstResp.getGstDetails());
			resp.setResponsecode(200);
			resp.setMsg("SUCCESS");
			
		}catch(Exception e){
			LOGGER.info("Exception occured in companyGstDetails due to : " + e, e);
			resp.setMsg("Searched Domain / Company ID does not exist");
			resp.setResponsecode(404);
		}
		
		LOGGER.debug("companyGstDetails END");
		return resp;
	}

	@Override
	public GenericResponse updateCompanyGst(GstDetailsUpdate gstUpd) {
		LOGGER.debug("updateCmpContInfo START");
		
		GenericResponse resp = new GenericResponse();
		try{
			String responseBody = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRq = mapper.writeValueAsString(gstUpd);
			
			HttpPost httpPost = new HttpPost(env.getProperty("update.company.gst"));
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30 * 1000).setConnectTimeout(30 * 1000).setConnectionRequestTimeout(30 * 1000).build();
			httpPost.setConfig(requestConfig);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			StringEntity requestEntity = new StringEntity(jsonRq, ContentType.create("application/json", "UTF-8"));
			httpPost.setHeader(null); //env.getProperty("ct.currency.scheduler.auth.key"), env.getProperty("ct.currency.scheduler.auth.value")
			       
			httpPost.setEntity(requestEntity);
			httpPost.setHeader("Content-type", "application/json");
			
			HttpClient httpClient =  HttpClientBuilder.create().build();
			responseBody = httpClient.execute(httpPost, responseHandler);
			  
			LOGGER.debug("HqCompanyServiceImpl updateCmpContInfo Response : " + responseBody);
			
			resp = mapper.readValue(responseBody, GenericResponse.class);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in updateCmpContInfo due to : " + e, e);
			resp.setMsg("Company GST Update failed");
			resp.setStatus(404);
		}
		
		LOGGER.debug("addEditCompConf END");
		return resp;
	}

	@Override
	public GenericResponse gstDelete(GstDelete gstDel) {
		LOGGER.debug("updateCmpContInfo START");
		
		GenericResponse resp = new GenericResponse();
		try{

			ObjectMapper mapper = new ObjectMapper();
			String jsonRq = mapper.writeValueAsString(gstDel);
			
			CloseableHttpClient httpclient = HttpClients.createDefault();
			 
	        String url = env.getProperty("company.gst.delete");
	 
	        HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(url);
	        StringEntity input = new StringEntity(jsonRq, ContentType.APPLICATION_JSON);
	         
	        httpDelete.setEntity(input);  
	 
	        CloseableHttpResponse response = httpclient.execute(httpDelete);
			  
			LOGGER.debug("HqCompanyServiceImpl updateCmpContInfo Response : " + response);
			
			resp.setMsg("SUCCESS");
			resp.setStatus(200);
			
		}catch(Exception e){
			LOGGER.info("Exception occured in updateCmpContInfo due to : " + e, e);
			resp.setMsg("Company GST Delete failed");
			resp.setStatus(404);
		}
		
		LOGGER.debug("addEditCompConf END");
		return resp;
	}
	
}
