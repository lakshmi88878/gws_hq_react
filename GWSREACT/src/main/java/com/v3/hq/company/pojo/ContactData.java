
package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactData implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("addresses")
    private List<Addresses> addresses = null;
    
    @JsonProperty("phone_numbers")
    private List<PhoneNumbers> phoneNumbers = null;
    
    @JsonProperty("other_details")
    private List<OtherDetails> otherDetails = null;
    
    @JsonProperty("websites")
    private List<Websites> websites = null;
    
    @JsonProperty("emails")
    private List<Emails> emails = null;
    
    @JsonProperty("whatsapp_details")
    private List<WhatsappDetails> whatsappDetails = null;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the addresses
	 */
	public List<Addresses> getAddresses() {
		return addresses;
	}

	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(List<Addresses> addresses) {
		this.addresses = addresses;
	}

	/**
	 * @return the phoneNumbers
	 */
	public List<PhoneNumbers> getPhoneNumbers() {
		return phoneNumbers;
	}

	/**
	 * @param phoneNumbers the phoneNumbers to set
	 */
	public void setPhoneNumbers(List<PhoneNumbers> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	/**
	 * @return the otherDetails
	 */
	public List<OtherDetails> getOtherDetails() {
		return otherDetails;
	}

	/**
	 * @param otherDetails the otherDetails to set
	 */
	public void setOtherDetails(List<OtherDetails> otherDetails) {
		this.otherDetails = otherDetails;
	}

	/**
	 * @return the websites
	 */
	public List<Websites> getWebsites() {
		return websites;
	}

	/**
	 * @param websites the websites to set
	 */
	public void setWebsites(List<Websites> websites) {
		this.websites = websites;
	}

	/**
	 * @return the emails
	 */
	public List<Emails> getEmails() {
		return emails;
	}

	/**
	 * @param emails the emails to set
	 */
	public void setEmails(List<Emails> emails) {
		this.emails = emails;
	}

	/**
	 * @return the whatsappDetails
	 */
	public List<WhatsappDetails> getWhatsappDetails() {
		return whatsappDetails;
	}

	/**
	 * @param whatsappDetails the whatsappDetails to set
	 */
	public void setWhatsappDetails(List<WhatsappDetails> whatsappDetails) {
		this.whatsappDetails = whatsappDetails;
	}

    
   
}
