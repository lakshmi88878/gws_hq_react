/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GstResponse implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
    
    @JsonProperty("gst_details")
    private List<GstDetails> gstDetails = null;
	
	@JsonProperty("responsecode")
	private Integer responsecode;
	
	@JsonProperty("msg")
	private String msg;

	public List<GstDetails> getGstDetails() {
		return gstDetails;
	}

	public void setGstDetails(List<GstDetails> gstDetails) {
		this.gstDetails = gstDetails;
	}

	public Integer getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(Integer responsecode) {
		this.responsecode = responsecode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
