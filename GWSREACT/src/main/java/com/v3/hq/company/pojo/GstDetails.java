/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GstDetails implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("gst_number")
	private String gstNumber;
	
	@JsonProperty("gst_holder_name")
	private String gstHolderName;
	
	@JsonProperty("gst_holder_address")
	private String gstHolderAddress;
	
	@JsonProperty("gst_holder_state_name")
	private String gstHolderStateName;
	
	@JsonProperty("gst_holder_state_code")
	private String gstHolderStateCode;
	
	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("linkable_id")
	private Integer linkableId;
	
	@JsonProperty("linkable_type")
	private String linkableType;
	
	@JsonProperty("updated_at")
	private String updatedAt;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the gstNumber
	 */
	public String getGstNumber() {
		return gstNumber;
	}

	/**
	 * @param gstNumber the gstNumber to set
	 */
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	/**
	 * @return the gstHolderName
	 */
	public String getGstHolderName() {
		return gstHolderName;
	}

	/**
	 * @param gstHolderName the gstHolderName to set
	 */
	public void setGstHolderName(String gstHolderName) {
		this.gstHolderName = gstHolderName;
	}

	/**
	 * @return the gstHolderAddress
	 */
	public String getGstHolderAddress() {
		return gstHolderAddress;
	}

	/**
	 * @param gstHolderAddress the gstHolderAddress to set
	 */
	public void setGstHolderAddress(String gstHolderAddress) {
		this.gstHolderAddress = gstHolderAddress;
	}

	/**
	 * @return the gstHolderStateName
	 */
	public String getGstHolderStateName() {
		return gstHolderStateName;
	}

	/**
	 * @param gstHolderStateName the gstHolderStateName to set
	 */
	public void setGstHolderStateName(String gstHolderStateName) {
		this.gstHolderStateName = gstHolderStateName;
	}

	/**
	 * @return the gstHolderStateCode
	 */
	public String getGstHolderStateCode() {
		return gstHolderStateCode;
	}

	/**
	 * @param gstHolderStateCode the gstHolderStateCode to set
	 */
	public void setGstHolderStateCode(String gstHolderStateCode) {
		this.gstHolderStateCode = gstHolderStateCode;
	}

	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the linkableId
	 */
	public Integer getLinkableId() {
		return linkableId;
	}

	/**
	 * @param linkableId the linkableId to set
	 */
	public void setLinkableId(Integer linkableId) {
		this.linkableId = linkableId;
	}

	/**
	 * @return the linkableType
	 */
	public String getLinkableType() {
		return linkableType;
	}

	/**
	 * @param linkableType the linkableType to set
	 */
	public void setLinkableType(String linkableType) {
		this.linkableType = linkableType;
	}

	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	

	
}
