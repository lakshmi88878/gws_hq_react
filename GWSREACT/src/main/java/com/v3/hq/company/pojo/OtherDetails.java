/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtherDetails implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;

}
