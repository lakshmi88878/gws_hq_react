
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyConfig implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;

    @JsonProperty("company_id")
    private Integer companyId;
    
    @JsonProperty("config_name")
    private String configName;
    
    @JsonProperty("config_value")
    private String configValue;
    
    @JsonProperty("created_by")
    private Object createdBy;
    
    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("updated_by")
    private Object updatedBy;

	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the configName
	 */
	public String getConfigName() {
		return configName;
	}

	/**
	 * @param configName the configName to set
	 */
	public void setConfigName(String configName) {
		this.configName = configName;
	}

	/**
	 * @return the configValue
	 */
	public String getConfigValue() {
		return configValue;
	}

	/**
	 * @param configValue the configValue to set
	 */
	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	/**
	 * @return the createdBy
	 */
	public Object getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Object createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the updatedBy
	 */
	public Object getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Object updatedBy) {
		this.updatedBy = updatedBy;
	}


    
}
