/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Addresses implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("category")
	private String category;
	
	@JsonProperty("city")
	private String city;
	
	@JsonProperty("city_id")
	private Integer cityId;
	
	@JsonProperty("contact_data_id")
	private Integer contactDataId;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("country_id")
	private Integer countryId;
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("pincode")
	private String pincode;
	
	@JsonProperty("seq_no")
	private Integer seqNo;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("state_id")
	private Object stateId;
	
	@JsonProperty("street_address")
	private String streetAddress;

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the cityId
	 */
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the contactDataId
	 */
	public Integer getContactDataId() {
		return contactDataId;
	}

	/**
	 * @param contactDataId the contactDataId to set
	 */
	public void setContactDataId(Integer contactDataId) {
		this.contactDataId = contactDataId;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the countryId
	 */
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the seqNo
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * @param seqNo the seqNo to set
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the stateId
	 */
	public Object getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Object stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	

	

}
