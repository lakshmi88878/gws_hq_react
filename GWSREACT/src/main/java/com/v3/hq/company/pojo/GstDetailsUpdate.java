/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GstDetailsUpdate implements Serializable {

	/**  *  */
	private static final long serialVersionUID = 1L;
    
    @JsonProperty("domain_name")
    private String domainName;
    
    @JsonProperty("id")
    private Integer id;	//Company ID
    
    @JsonProperty("gst_details")
    private List<GstDetails> gstDetails = null;
    

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public List<GstDetails> getGstDetails() {
		return gstDetails;
	}

	public void setGstDetails(List<GstDetails> gstDetails) {
		this.gstDetails = gstDetails;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
