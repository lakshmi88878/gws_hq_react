/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GstDelete implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
    
    @JsonProperty("id")
    private Integer id;	//Company ID
    
    @JsonProperty("domain_name")
    private String domainName;
    
    @JsonProperty("gst_details")
    private List<GstDetails> gstDetails = null;

}
