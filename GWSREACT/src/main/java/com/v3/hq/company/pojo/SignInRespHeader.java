package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignInRespHeader implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "status")
	private String status;
	
	@JsonProperty(value = "redirect")
	private String redirect;
	
	@JsonProperty(value = "ct-auth")
	private String ctAuthValue;
	
	@JsonProperty(value = "usermisc")
	private String usermiscValue;
	
	@JsonProperty(value = "userid")
	private String useridValue;
	
	@JsonProperty(value = "ct-auth-preferences")
	private String preferencesValue;
	
	@JsonProperty(value = "currency-pref")
	private String currencyprefValue;
	
	@JsonProperty(value = "_session_id")
	private String sessionId;
	
	@JsonProperty(value = "Apache")
	private String apacheValue;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCtAuthValue() {
		return ctAuthValue;
	}

	public void setCtAuthValue(String ctAuthValue) {
		this.ctAuthValue = ctAuthValue;
	}

	public String getUsermiscValue() {
		return usermiscValue;
	}

	public void setUsermiscValue(String usermiscValue) {
		this.usermiscValue = usermiscValue;
	}

	public String getUseridValue() {
		return useridValue;
	}

	public void setUseridValue(String useridValue) {
		this.useridValue = useridValue;
	}

	public String getPreferencesValue() {
		return preferencesValue;
	}

	public void setPreferencesValue(String preferencesValue) {
		this.preferencesValue = preferencesValue;
	}

	public String getCurrencyprefValue() {
		return currencyprefValue;
	}

	public void setCurrencyprefValue(String currencyprefValue) {
		this.currencyprefValue = currencyprefValue;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getApacheValue() {
		return apacheValue;
	}

	public void setApacheValue(String apacheValue) {
		this.apacheValue = apacheValue;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}
	
	

}
