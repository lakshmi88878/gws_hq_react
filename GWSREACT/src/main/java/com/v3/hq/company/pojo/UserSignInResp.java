package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSignInResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "redirect")
	private String redirect;
	
	@JsonProperty(value = "fault")
	private UsrSignInFault fault;
	

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public UsrSignInFault getFault() {
		return fault;
	}

	public void setFault(UsrSignInFault fault) {
		this.fault = fault;
	}
	
	
	
	
	
	
	

}
