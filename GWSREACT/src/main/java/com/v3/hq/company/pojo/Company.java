package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;

    @JsonProperty("about")
    private String about;
    
    @JsonProperty("address1")
    private String address1;
    
    @JsonProperty("address2")
    private String address2;
    
    @JsonProperty("annual_travel_budget")
    private String annualTravelBudget;
    
    @JsonProperty("channel_type")
    private Integer channelType;
    
    @JsonProperty("city")
    private String city;
    
    @JsonProperty("company_website")
    private String companyWebsite;
    
    @JsonProperty("country")
    private String country;
    
    @JsonProperty("created_at")
    private String createdAt;
    
    @JsonProperty("cust_service_email")
    private String custServiceEmail;
    
    @JsonProperty("cust_service_phone")
    private String custServicePhone;
    
    @JsonProperty("domain_name")
    private String domainName;
    
    @JsonProperty("effective_from")
    private String effectiveFrom;
    
    @JsonProperty("effective_to")
    private String effectiveTo;
    
    @JsonProperty("group_name")
    private String groupName;
    
    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("pan_card_details")
    private String panCardDetails;
    
    @JsonProperty("service_no")
    private String serviceNo;
    
    @JsonProperty("site_description")
    private String siteDescription;
    
    @JsonProperty("site_name")
    private String siteName;
    
    @JsonProperty("state")
    private String state;
    
    @JsonProperty("status")
    private Integer status;
    
    @JsonProperty("tin_number")
    private String tinNumber;
    
    @JsonProperty("updated_at")
    private String updatedAt;
    
    @JsonProperty("zip")
    private String zip;
    
    @JsonProperty("channel_type_name")
    private String channelTypeName;
    
    @JsonProperty("contact_data")
    private ContactData contactData;
    
    @JsonProperty("departments")
    private List<Departments> departments = null;
    
    @JsonProperty("company_configs")
    private List<CompanyConfig> companyConfigs = null;
    
    @JsonProperty("company_tourcodes")
    private List<CompanyTourcodes> companyTourcodes = null;
    
    @JsonProperty("company_tags")
    private List<CompanyTags> companyTags = null;
    
    @JsonProperty("gst_details")
    private List<GstDetails> gstDetails = null;
    
    @JsonProperty("resources")
    private List<Resources> resources = null;
    
    @JsonProperty("company_logo")
    private String companyLogo;
    
    @JsonProperty("deposit_accounts")
    private List<DepositAccounts> depositAccounts = null;

	/**
	 * @return the about
	 */
	public String getAbout() {
		return about;
	}

	/**
	 * @param about the about to set
	 */
	public void setAbout(String about) {
		this.about = about;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the annualTravelBudget
	 */
	public String getAnnualTravelBudget() {
		return annualTravelBudget;
	}

	/**
	 * @param annualTravelBudget the annualTravelBudget to set
	 */
	public void setAnnualTravelBudget(String annualTravelBudget) {
		this.annualTravelBudget = annualTravelBudget;
	}

	/**
	 * @return the channelType
	 */
	public Integer getChannelType() {
		return channelType;
	}

	/**
	 * @param channelType the channelType to set
	 */
	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the companyWebsite
	 */
	public String getCompanyWebsite() {
		return companyWebsite;
	}

	/**
	 * @param companyWebsite the companyWebsite to set
	 */
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the custServiceEmail
	 */
	public String getCustServiceEmail() {
		return custServiceEmail;
	}

	/**
	 * @param custServiceEmail the custServiceEmail to set
	 */
	public void setCustServiceEmail(String custServiceEmail) {
		this.custServiceEmail = custServiceEmail;
	}

	/**
	 * @return the custServicePhone
	 */
	public String getCustServicePhone() {
		return custServicePhone;
	}

	/**
	 * @param custServicePhone the custServicePhone to set
	 */
	public void setCustServicePhone(String custServicePhone) {
		this.custServicePhone = custServicePhone;
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return the effectiveFrom
	 */
	public String getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom the effectiveFrom to set
	 */
	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 */
	public String getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @param effectiveTo the effectiveTo to set
	 */
	public void setEffectiveTo(String effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the panCardDetails
	 */
	public String getPanCardDetails() {
		return panCardDetails;
	}

	/**
	 * @param panCardDetails the panCardDetails to set
	 */
	public void setPanCardDetails(String panCardDetails) {
		this.panCardDetails = panCardDetails;
	}

	/**
	 * @return the serviceNo
	 */
	public String getServiceNo() {
		return serviceNo;
	}

	/**
	 * @param serviceNo the serviceNo to set
	 */
	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}

	/**
	 * @return the siteDescription
	 */
	public String getSiteDescription() {
		return siteDescription;
	}

	/**
	 * @param siteDescription the siteDescription to set
	 */
	public void setSiteDescription(String siteDescription) {
		this.siteDescription = siteDescription;
	}

	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * @param siteName the siteName to set
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the tinNumber
	 */
	public String getTinNumber() {
		return tinNumber;
	}

	/**
	 * @param tinNumber the tinNumber to set
	 */
	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the channelTypeName
	 */
	public String getChannelTypeName() {
		return channelTypeName;
	}

	/**
	 * @param channelTypeName the channelTypeName to set
	 */
	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}

	/**
	 * @return the contactData
	 */
	public ContactData getContactData() {
		return contactData;
	}

	/**
	 * @param contactData the contactData to set
	 */
	public void setContactData(ContactData contactData) {
		this.contactData = contactData;
	}

	/**
	 * @return the departments
	 */
	public List<Departments> getDepartments() {
		return departments;
	}

	/**
	 * @param departments the departments to set
	 */
	public void setDepartments(List<Departments> departments) {
		this.departments = departments;
	}

	/**
	 * @return the companyConfigs
	 */
	public List<CompanyConfig> getCompanyConfigs() {
		return companyConfigs;
	}

	/**
	 * @param companyConfigs the companyConfigs to set
	 */
	public void setCompanyConfigs(List<CompanyConfig> companyConfigs) {
		this.companyConfigs = companyConfigs;
	}

	/**
	 * @return the companyTourcodes
	 */
	public List<CompanyTourcodes> getCompanyTourcodes() {
		return companyTourcodes;
	}

	/**
	 * @param companyTourcodes the companyTourcodes to set
	 */
	public void setCompanyTourcodes(List<CompanyTourcodes> companyTourcodes) {
		this.companyTourcodes = companyTourcodes;
	}

	/**
	 * @return the companyTags
	 */
	public List<CompanyTags> getCompanyTags() {
		return companyTags;
	}

	/**
	 * @param companyTags the companyTags to set
	 */
	public void setCompanyTags(List<CompanyTags> companyTags) {
		this.companyTags = companyTags;
	}

	/**
	 * @return the gstDetails
	 */
	public List<GstDetails> getGstDetails() {
		return gstDetails;
	}

	/**
	 * @param gstDetails the gstDetails to set
	 */
	public void setGstDetails(List<GstDetails> gstDetails) {
		this.gstDetails = gstDetails;
	}

	/**
	 * @return the resources
	 */
	public List<Resources> getResources() {
		return resources;
	}

	/**
	 * @param resources the resources to set
	 */
	public void setResources(List<Resources> resources) {
		this.resources = resources;
	}

	/**
	 * @return the companyLogo
	 */
	public String getCompanyLogo() {
		return companyLogo;
	}

	/**
	 * @param companyLogo the companyLogo to set
	 */
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	/**
	 * @return the depositAccounts
	 */
	public List<DepositAccounts> getDepositAccounts() {
		return depositAccounts;
	}

	/**
	 * @param depositAccounts the depositAccounts to set
	 */
	public void setDepositAccounts(List<DepositAccounts> depositAccounts) {
		this.depositAccounts = depositAccounts;
	}


    

}
