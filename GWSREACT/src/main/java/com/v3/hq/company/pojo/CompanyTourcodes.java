/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyTourcodes implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
	@JsonProperty("id")
	private Long id;

	@JsonProperty("company_id")
	private Long company_id;

	@JsonProperty("airline_code")
	private String airline_code;

	@JsonProperty("tourcode")
	private String tourcode;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the company_id
	 */
	public Long getCompany_id() {
		return company_id;
	}

	/**
	 * @param company_id the company_id to set
	 */
	public void setCompany_id(Long company_id) {
		this.company_id = company_id;
	}

	/**
	 * @return the airline_code
	 */
	public String getAirline_code() {
		return airline_code;
	}

	/**
	 * @param airline_code the airline_code to set
	 */
	public void setAirline_code(String airline_code) {
		this.airline_code = airline_code;
	}

	/**
	 * @return the tourcode
	 */
	public String getTourcode() {
		return tourcode;
	}

	/**
	 * @param tourcode the tourcode to set
	 */
	public void setTourcode(String tourcode) {
		this.tourcode = tourcode;
	}

	

}
