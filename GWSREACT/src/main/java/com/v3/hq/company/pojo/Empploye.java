package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Empploye implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
 @JsonProperty(value = "addEmploye")
 private AddEmploye addEmploye;

public AddEmploye getAddEmploye() {
	return addEmploye;
}

public void setAddEmploye(AddEmploye addEmploye) {
	this.addEmploye = addEmploye;
}
 
 

}
