/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepositAccounts implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
    
    @JsonProperty("id")
    private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
