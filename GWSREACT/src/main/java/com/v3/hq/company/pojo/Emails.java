/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Emails implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("category")
	private String category;
	
	@JsonProperty("contact_data_id")
	private Integer contactDataId;
	
	@JsonProperty("email_id")
	private String emailId;
	
	@JsonProperty("phoneNo")
	private Integer phoneNo;
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("seq_no")
	private Integer seqNo;

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the contactDataId
	 */
	public Integer getContactDataId() {
		return contactDataId;
	}

	/**
	 * @param contactDataId the contactDataId to set
	 */
	public void setContactDataId(Integer contactDataId) {
		this.contactDataId = contactDataId;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the phoneNo
	 */
	public Integer getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo the phoneNo to set
	 */
	public void setPhoneNo(Integer phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the seqNo
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * @param seqNo the seqNo to set
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	

	

}
