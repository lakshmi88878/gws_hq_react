/**
 * 
 */
package com.v3.hq.company.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rakesh Korwar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Resources implements Serializable{

	/**  *  */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("linkable_id")
	private Integer linkableId;
	
	@JsonProperty("linkable_type")
	private String linkableType;
	
	@JsonProperty("resource_name")
	private String resourceName;
	
	@JsonProperty("resource_value")
	private String resourceValue;
	
	@JsonProperty("resource_value_category")
	private String resourceValueCategory;
	
	@JsonProperty("resource_varchar")
	private String resourceVarchar;
	
	@JsonProperty("restype_id")
	private Integer restypeId;
	
	@JsonProperty("resource_type")
	private String resourceType;
	
	@JsonProperty("artifacts")
	private List<Artifacts> artifacts = null;
	
	@JsonProperty("resouce_blob")
	private String resouceBlob;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the linkableId
	 */
	public Integer getLinkableId() {
		return linkableId;
	}

	/**
	 * @param linkableId the linkableId to set
	 */
	public void setLinkableId(Integer linkableId) {
		this.linkableId = linkableId;
	}

	/**
	 * @return the linkableType
	 */
	public String getLinkableType() {
		return linkableType;
	}

	/**
	 * @param linkableType the linkableType to set
	 */
	public void setLinkableType(String linkableType) {
		this.linkableType = linkableType;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the resourceValue
	 */
	public String getResourceValue() {
		return resourceValue;
	}

	/**
	 * @param resourceValue the resourceValue to set
	 */
	public void setResourceValue(String resourceValue) {
		this.resourceValue = resourceValue;
	}

	/**
	 * @return the resourceValueCategory
	 */
	public String getResourceValueCategory() {
		return resourceValueCategory;
	}

	/**
	 * @param resourceValueCategory the resourceValueCategory to set
	 */
	public void setResourceValueCategory(String resourceValueCategory) {
		this.resourceValueCategory = resourceValueCategory;
	}

	/**
	 * @return the resourceVarchar
	 */
	public String getResourceVarchar() {
		return resourceVarchar;
	}

	/**
	 * @param resourceVarchar the resourceVarchar to set
	 */
	public void setResourceVarchar(String resourceVarchar) {
		this.resourceVarchar = resourceVarchar;
	}

	/**
	 * @return the restypeId
	 */
	public Integer getRestypeId() {
		return restypeId;
	}

	/**
	 * @param restypeId the restypeId to set
	 */
	public void setRestypeId(Integer restypeId) {
		this.restypeId = restypeId;
	}

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the artifacts
	 */
	public List<Artifacts> getArtifacts() {
		return artifacts;
	}

	/**
	 * @param artifacts the artifacts to set
	 */
	public void setArtifacts(List<Artifacts> artifacts) {
		this.artifacts = artifacts;
	}

	/**
	 * @return the resouceBlob
	 */
	public String getResouceBlob() {
		return resouceBlob;
	}

	/**
	 * @param resouceBlob the resouceBlob to set
	 */
	public void setResouceBlob(String resouceBlob) {
		this.resouceBlob = resouceBlob;
	}
	
	
	

	

}
